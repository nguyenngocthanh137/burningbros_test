import React from "react";
import { Product } from "../type";

type Props = {
	data: Product;
};

const ProductCard: React.FC<Props> = ({ data }) => {
	return (
		<div className="product">
			<img src={data?.thumbnail} alt="product" />
			<p>{data?.title}</p>
			<p>{data?.price}</p>
		</div>
	);
};

export default ProductCard;
