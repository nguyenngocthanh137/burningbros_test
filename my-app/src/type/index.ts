export interface DataRequest {
    limit: number,
    search: string,
}

export interface Product {
    title: string,
    price: number,
    thumbnail: string
}

export interface DataResponse {
    limit: number,
    skip: number,
    total: number,
    products: Product[]
}

export interface ParamsGetProduct {
    searchText: string;
    number: number;
};
