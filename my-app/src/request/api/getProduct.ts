import request from "../index";
import { DataRequest } from "../../type";

export const getProductApi = async (data: DataRequest) => {
    try {
        const res = await request.get('/products/search', { params: { limit: data?.limit, q: data?.search } })
        return res?.data
    }
    catch (error) {
        console.log(error)
    }
}