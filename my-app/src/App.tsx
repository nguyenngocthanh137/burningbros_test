import { ChangeEvent, useCallback, useEffect, useRef, useState } from "react";
import "./App.css";
import "./css/main.css";
import { getProductApi } from "./request/api/getProduct";
import { DataResponse, ParamsGetProduct } from "./type";

import ProductCard from "./components/ProductCard";
import Loading from "./components/Loading";

function App() {
	const [productInfo, setProductInfo] = useState<DataResponse>();
	const [number, setNumber] = useState(1);
	const [searchText, setSearchText] = useState("");
	const [loading, setLoading] = useState(false);

	const limit = 20;

	const timer = useRef<ReturnType<typeof setTimeout>>();

	const handleSearchProduct = async (e: ChangeEvent) => {
		clearTimeout(timer.current);

		const target = e.target as HTMLInputElement;
		setSearchText(target.value);
		setNumber(1);

		timer.current = setTimeout(() => {
			handleGetProduct({ searchText: target.value, number: 1 });
		}, 350);
	};

	const handleGetProduct = async (data: ParamsGetProduct) => {
		setLoading(true);

		let newLimit = limit;

		if (productInfo) {
			if (productInfo?.products?.length < productInfo?.total) {
				const quantity = productInfo?.total - productInfo?.products?.length;

				if (quantity >= limit) {
					newLimit = data?.number * limit;
				} else {
					newLimit = productInfo?.total;
				}
			}
		}

		const res = await getProductApi({
			search: data?.searchText,
			limit: newLimit,
		});

		setProductInfo(res);

		setLoading(false);
	};

	useEffect(() => {
		handleGetProduct({ searchText, number });
	}, []);

	const handleScroll = (e: Event) => {
		const target = e.target as HTMLDivElement;
		const bottom =
			target.scrollHeight - target.scrollTop === target.clientHeight;
		if (bottom) {
			if (productInfo && productInfo?.products?.length < productInfo?.total) {
				const newNumber = number + 1;
				setNumber(newNumber);
				handleGetProduct({ searchText, number: newNumber });
			}
		}
	};

	useEffect(() => {
		const appElement = document.getElementById("app");

		if (appElement) {
			appElement.addEventListener("scroll", handleScroll);
		}

		return () => appElement?.removeEventListener("scroll", handleScroll);
	}, [productInfo]);

	return (
		<div id="app">
			{loading && <Loading />}
			<h1>Burningbros Test</h1>
			<input
				onChange={(e: ChangeEvent) => handleSearchProduct(e)}
				placeholder="Search Product"
			/>
			<div className="product-list">
				{productInfo?.products?.map((x, index) => (
					<ProductCard key={index} data={x} />
				))}
			</div>
		</div>
	);
}

export default App;
